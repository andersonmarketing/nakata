<?php
/**
 * The Template Part for displaying the precontent.
 *
 * For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package G1_Framework
 * @subpackage G1_Theme03
 * @since G1_Theme03 1.0.0
 */

// Prevent direct script access
if ( !defined('ABSPATH') )
    die ( 'No direct script access allowed' );
?>
<?php
    ob_start();
    do_action( 'g1_precontent' );
    $g1_precontent = trim( ob_get_clean() );
?>

<!-- BEGIN #g1-precontent -->
  
<div id="g1-precontent">
       
        
   <?php  
   $list = get_post_ancestors( $post );
  //print_r($list);
$id = get_the_ID();
   $url = wp_get_attachment_url( get_post_thumbnail_id($post_id) );
   if ($list[0] == 24)
   {
        if($url != null)
        {
             echo '<div class="g1-layout-inner">';
         G1_Theme()->render_site_id();
         echo '</div>';

        echo '<div class="feature_img">'; 

       $banner_text = get_post_meta( $id, 'product_banner_text',true );
if( ! empty( $banner_text ) ) {
  echo '<div class="product_banner_text">'.$banner_text.'</div>';

} 

        echo '<img src="'.$url.'">';
        echo '</div>';
        }
        else
        { 
        }
   }
   else
        {
          echo '<div class="slider_shadow"></div>'; 
          echo '<div class="g1-layout-inner">';
         G1_Theme()->render_site_id();
         echo '</div>';
          echo $g1_precontent;
        }
   ?> 
 

    <?php get_template_part( 'template-parts/g1_background', 'precontent' ); ?>
</div>
<!-- END #g1-precontent -->
