<?php
/*
Template Name: Left and right sidebar
*/

get_header(); ?>
<div class="product_page">
<?php  echo '<div id="left_menu">'.do_shortcode('[widget id="nav_menu-2"]').'</div>';

 ?>

<div id="content" class="widecolumn">
    <?php if (have_posts()) : while (have_posts()) : the_post();?>
    <div class="post">
        <h2 id="post-<?php the_ID(); ?>"><?php /*the_title();*/?></h2>
        <div class="entrytext">
            <?php the_content('<p class="serif">Read the rest of this page »</p>'); ?>
        </div>
    </div>
    <?php endwhile; endif; ?>
<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
</div>
<div class="border_left"></div>
<?php echo '<div id="right_menu">'.do_shortcode('[widget id="text-2"]').'</div>';?>
</div>
<?php get_footer(); ?>
